package com.hendisantika.blog

import org.springframework.boot.context.properties.ConstructorBinding

/**
 * Created by IntelliJ IDEA.
 * Project : blog
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 03/03/20
 * Time: 19.44
 */
@ConstructorBinding
//@ConfigurationProperties("blog")
data class BlogProperties(var title: String, val banner: Banner) {
    data class Banner(val title: String? = null, val content: String)
}